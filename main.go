package main

import(
	"fmt"
	"net"
	"sort"
)
//prueba
var target string

func worker(ports, resutls chan int){
	for p := range ports{
		address := fmt.Sprintf("%v:%d", target, p)
		conn, err :=net.Dial("tcp", address)
		if err != nil{
			resutls <- 0 
			continue
		}
		conn.Close()
		resutls <- p
	}
}

func main(){
	
	fmt.Println("Target: ")
	fmt.Scanf("%v", &target)
	ports := make(chan int, 100)
	resutls := make(chan int)
	var openports []int

	for i := 0; i < cap(ports); i++{
		go worker(ports, resutls)
	}

	go func(){
		for i := 1; i <= 65535; i++{
			ports <- i
		}
	}()

	for i := 0; i <65535; i++{
		port := <-resutls
		if port != 0{
			openports =append(openports, port)
		}
	}

	close(ports)
	close(resutls)
	sort.Ints(openports)
	for _, port := range openports{
		fmt.Printf("%d open\n", port)
	}
	fmt.Printf("\nScan Finished.\n")
}
